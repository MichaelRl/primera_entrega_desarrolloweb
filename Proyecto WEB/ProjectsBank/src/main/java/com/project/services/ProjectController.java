package com.project.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.project.entities.Faculty;
import com.project.entities.Project;
import com.project.entities.User;
import com.project.repositories.ProjectRepository;

@RestController
public class ProjectController {

	@Autowired
	private ProjectRepository projectRepositoryDAO;

	@RequestMapping("/createProject")
	public String createProject(@RequestParam long projectId, @RequestParam String projectName,
			@RequestParam String projectDescription, @RequestParam String projectFaculty,
			@RequestParam String projectLeader, @RequestParam User projectLeaderId,
			@RequestParam String projectCategory, @RequestParam Date projectDate, @RequestParam Faculty faculty) {
		Project newProject = new Project();
		newProject.setProjectId(projectId);
		newProject.setProjectName(projectName);
		newProject.setProjectDescription(projectDescription);
		newProject.setProjectFaculty(projectFaculty);
		newProject.setProjectLeader(projectLeader);
		newProject.setUser(projectLeaderId);
		newProject.setProjectCategory(projectCategory);
		newProject.setProjectDate(projectDate);
		newProject.setFaculty(faculty);
		projectRepositoryDAO.save(newProject);
		return "Project Saved";
	}

	@RequestMapping("/deleteProjectById")
	public String deleteById(@RequestParam Long id) {
		projectRepositoryDAO.deleteById(id);
		return "ID = " + id + " - Project Deleted";
	}

	@RequestMapping("/getProjectById")
	public Project findById(@RequestParam long id) {
		return projectRepositoryDAO.findById(id);
	}

	@RequestMapping("/getProjects")
	public List<Project> findAll() {
		return projectRepositoryDAO.findAll();
	}

	@RequestMapping("/projectExistsById")
	public boolean existsById(@RequestParam Long id) {
		if (projectRepositoryDAO.existsById(id)) {
			return true;
		} else {
			return false;
		}
	}

	@RequestMapping("/projectCount")
	public long count() {
		return projectRepositoryDAO.findAll().size();
	}

	@RequestMapping("/deleteAllProjects")
	public void deleteAll() {
		projectRepositoryDAO.deleteAll();
	}

	public Iterable<Project> findAllById(Iterable<Long> ids) {
		return null;
	}

	public void delete(Project entity) {

	}

}
