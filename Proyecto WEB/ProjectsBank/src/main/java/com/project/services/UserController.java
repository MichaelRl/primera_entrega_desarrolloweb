package com.project.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.project.entities.User;
import com.project.repositories.UserRepository;

@RestController
public class UserController {

	@Autowired
	private UserRepository userRepositoryDAO;

	@RequestMapping("/createUser")
	public String createUser(@RequestParam long userId, @RequestParam String userName,
			@RequestParam String userLastName, @RequestParam String userEmail, @RequestParam String userPassword,
			@RequestParam String userDocNum, @RequestParam String userCod) {
		User newUser = new User();
		newUser.setUserId(userId);
		newUser.setUserName(userName);
		newUser.setUserLastName(userLastName);
		newUser.setUserEmail(userEmail);
		newUser.setUserPassword(userPassword);
		newUser.setUserDocNum(userDocNum);
		newUser.setUserCod(userCod);
		userRepositoryDAO.save(newUser);
		return "User Saved";
	}

	@RequestMapping("/deleteAllUsers")
	public void deleteAll() {
		userRepositoryDAO.deleteAll();
	}

	@RequestMapping("/deleteUserById")
	public String deleteById(@RequestParam long id) {
		userRepositoryDAO.deleteById(id);
		return "ID = " + id + " - User Deleted";
	}

	@RequestMapping("/getUserById")
	public User findById(@RequestParam long id) {
		return userRepositoryDAO.findById(id);
	}

	@RequestMapping("/getUsers")
	public List<User> findAll() {
		return userRepositoryDAO.findAll();
	}

	@RequestMapping("/userExistsById")
	public boolean existsById(@RequestParam Long id) {
		if (userRepositoryDAO.existsById(id)) {
			return true;
		} else {
			return false;
		}
	}

	@RequestMapping("/userCount")
	public long count() {
		return userRepositoryDAO.findAll().size();
	}

	public Iterable<User> findAllById(Iterable<Long> ids) {
		return null;
	}

	public void delete(User entity) {

	}

}
