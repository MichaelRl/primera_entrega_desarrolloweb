package com.project.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.project.entities.Faculty;
import com.project.repositories.FacultyRepository;

@RestController
public class FacultyController {

	@Autowired
	private FacultyRepository facultyRepositoryDAO;

	@RequestMapping("/createFaculty")
	public String createFaculty(@RequestParam long facultyId, @RequestParam String facultyName,
			@RequestParam String facultyDescription) {
		Faculty newFaculty = new Faculty();
		newFaculty.setFacultyId(facultyId);
		newFaculty.setFacultyName(facultyName);
		newFaculty.setFacultyDescription(facultyDescription);
		facultyRepositoryDAO.save(newFaculty);
		return "Faculty Saved";
	}

	@RequestMapping("/deleteFacultyById")
	public String deleteById(@RequestParam Long id) {
		facultyRepositoryDAO.deleteById(id);
		return "ID = " + id + " - Faculty Deleted";
	}

	@RequestMapping("/facultyExistsById")
	public boolean existsById(@RequestParam Long id) {
		if (facultyRepositoryDAO.existsById(id)) {
			return true;
		} else {
			return false;
		}
	}

	@RequestMapping("/deleteAllFaculties")
	public void deleteAll() {
		facultyRepositoryDAO.deleteAll();
	}

	@RequestMapping("/getFacultyById")
	public Faculty findById(@RequestParam long id) {
		return facultyRepositoryDAO.findById(id);
	}

	@RequestMapping("/getFaculties")
	public List<Faculty> findAll() {
		return facultyRepositoryDAO.findAll();
	}

	@RequestMapping("/facultyCount")
	public long count() {
		return facultyRepositoryDAO.findAll().size();
	}

	public Iterable<Faculty> findAllById(Iterable<Long> ids) {
		return null;
	}

	public void delete(Faculty entity) {

	}

}
