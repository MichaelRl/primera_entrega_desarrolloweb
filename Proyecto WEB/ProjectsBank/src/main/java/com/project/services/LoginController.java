package com.project.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.project.entities.Login;
import com.project.entities.User;
import com.project.repositories.LoginRepository;

@RestController
public class LoginController {

	@Autowired
	private LoginRepository loginRepositoryDAO;

	@RequestMapping("/createLogin")
	public String createLogin(@RequestParam long loginId, @RequestParam Date loginDate, @RequestParam User loginUserId,
			@RequestParam String loginEmail, @RequestParam String loginPassword) {
		Login newLogin = new Login();
		newLogin.setLoginId(loginId);
		newLogin.setLoginDate(loginDate);
		newLogin.setUser(loginUserId);
		newLogin.setLoginEmail(loginEmail);
		newLogin.setLoginPassword(loginPassword);
		loginRepositoryDAO.save(newLogin);
		return "Login Saved";
	}

	@RequestMapping("/deleteLoginById")
	public String deleteById(@RequestParam Long id) {
		loginRepositoryDAO.deleteById(id);
		return "ID = " + id + " - Login Deleted";
	}

	@RequestMapping("/getLoginById")
	public Login findById(@RequestParam long id) {
		return loginRepositoryDAO.findById(id);
	}

	@RequestMapping("/getLogins")
	public List<Login> findAll() {
		return loginRepositoryDAO.findAll();
	}

	@RequestMapping("/loginExistsById")
	public boolean existsById(@RequestParam Long id) {
		if (loginRepositoryDAO.existsById(id)) {
			return true;
		} else {
			return false;
		}
	}

	@RequestMapping("/loginCount")
	public long count() {
		return loginRepositoryDAO.findAll().size();
	}

	@RequestMapping("/deleteAllLogins")
	public void deleteAll() {
		loginRepositoryDAO.deleteAll();
	}

	public Iterable<Login> findAllById(Iterable<Long> ids) {
		return null;
	}

	public void delete(Login entity) {

	}

}
