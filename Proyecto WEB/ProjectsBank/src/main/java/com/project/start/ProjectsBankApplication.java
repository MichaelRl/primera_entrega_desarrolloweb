package com.project.start;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan (basePackages="com.project.services,com.project.repositories,com.project.entities")
@EntityScan(basePackages = {"com.project.entities"}) 
@EnableJpaRepositories ("com.project.repositories")

public class ProjectsBankApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectsBankApplication.class, args);
	}

}