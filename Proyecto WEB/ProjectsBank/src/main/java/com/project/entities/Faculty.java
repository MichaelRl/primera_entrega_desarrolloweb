package com.project.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the faculty database table.
 * 
 */
@Entity
@NamedQuery(name = "Faculty.findAll", query = "SELECT f FROM Faculty f")
public class Faculty implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "facultyId")
	private long facultyId;

	@Column(name = "facultyName")
	private String facultyName;

	@Column(name = "facultyDescription")
	private String facultyDescription;

	// bi-directional many-to-one association to Project
	@OneToMany(mappedBy = "faculty")
	private List<Project> projects;

	public Faculty() {
	}

	public long getFacultyId() {
		return this.facultyId;
	}

	public void setFacultyId(long facultyId) {
		this.facultyId = facultyId;
	}

	public String getFacultyDescription() {
		return this.facultyDescription;
	}

	public void setFacultyDescription(String facultyDescription) {
		this.facultyDescription = facultyDescription;
	}

	public String getFacultyName() {
		return this.facultyName;
	}

	public void setFacultyName(String facultyName) {
		this.facultyName = facultyName;
	}

	public List<Project> getProjects() {
		return this.projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

	public Project addProject(Project project) {
		getProjects().add(project);
		project.setFaculty(this);

		return project;
	}

	public Project removeProject(Project project) {
		getProjects().remove(project);
		project.setFaculty(null);

		return project;
	}

}