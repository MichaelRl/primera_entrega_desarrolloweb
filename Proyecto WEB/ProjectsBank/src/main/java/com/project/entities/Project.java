package com.project.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the project database table.
 * 
 */
@Entity
@NamedQuery(name = "Project.findAll", query = "SELECT p FROM Project p")
public class Project implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "projectId")
	private long projectId;

	@Column(name = "projectName")
	private String projectName;

	@Column(name = "projectDescription")
	private String projectDescription;

	@Column(name = "projectFaculty")
	private String projectFaculty;

	@Column(name = "projectLeader")
	private String projectLeader;

	// bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name = "projectLeaderId")
	private User user;

	@Column(name = "projectCategory")
	private String projectCategory;

	@Temporal(TemporalType.DATE)
	private Date projectDate;

	// bi-directional many-to-one association to Faculty
	@ManyToOne
	@JoinColumn(name = "projectFacultyId")
	private Faculty faculty;

	public Project() {
	}

	public long getProjectId() {
		return this.projectId;
	}

	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}

	public String getProjectCategory() {
		return this.projectCategory;
	}

	public void setProjectCategory(String projectCategory) {
		this.projectCategory = projectCategory;
	}

	public Date getProjectDate() {
		return this.projectDate;
	}

	public void setProjectDate(Date projectDate) {
		this.projectDate = projectDate;
	}

	public String getProjectDescription() {
		return this.projectDescription;
	}

	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}

	public String getProjectFaculty() {
		return this.projectFaculty;
	}

	public void setProjectFaculty(String projectFaculty) {
		this.projectFaculty = projectFaculty;
	}

	public String getProjectLeader() {
		return this.projectLeader;
	}

	public void setProjectLeader(String projectLeader) {
		this.projectLeader = projectLeader;
	}

	public String getProjectName() {
		return this.projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Faculty getFaculty() {
		return this.faculty;
	}

	public void setFaculty(Faculty faculty) {
		this.faculty = faculty;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}