package com.project.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.project.entities.Login;

@Repository
public interface LoginRepository extends CrudRepository<Login, Long>{

	public Login findById(long id); // Get Login By Id

	public List<Login> findAll(); // Get All Logins
}