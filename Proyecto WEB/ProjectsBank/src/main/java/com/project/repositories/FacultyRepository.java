package com.project.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.project.entities.Faculty;

@Repository
public interface FacultyRepository extends CrudRepository<Faculty, Long> {

	public Faculty findById(long id); // Get Faculties By Id

	public List<Faculty> findAll(); // Get All Faculties

}
