package com.project.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.project.entities.Project;

@Repository
public interface ProjectRepository extends CrudRepository<Project, Long> {

	public Project findById(long id); // Get Project By Id

	public List<Project> findAll(); // Get All Projects
}
