package com.project.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.project.entities.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

	public User findById(long id); // Get Users By Id

	public List<User> findAll(); // Get All Users
	
}
